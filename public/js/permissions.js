Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');

new Vue({
    el: '#id_permission',

    data: {
        newPermission: {
            name: '',
            label: ''
        },

        submitted: false,
        role_id: ''
    },

    computed: {
        errors: function () {
            for (var key in this.newPermission) {
                if (! this.newPermission[key]) return true;
            }
            return false;
        }
    },

    ready: function() {
        this.fetchPermissions();
    },

    methods: {
        fetchPermissions: function() {
            // permission is array from database
            this.$http.get('api/permissions', function (permission) {
                this.$set('permission', permission);
            });
        },

        onSubmitForm: function(e) {
            // prevent the default action
            e.preventDefault();
            // variable need for method post
            var permission = this.newPermission;
            // add new permission to permission array
            this.permission.push(this.newPermission);
            // reset input values
            this.newPermission = { name: '', label: ''};
            // show added message
            this.submitted = true;
            // send post ajax request
            this.$http.post('api/permissions', permission);
        },

        addRole: function(pid) {
            this.role_id = pid;
        },

        saveRoles: function(e) {
            var rpid = [];
            role_id = this.role_id;
            $('#sample_2').find('input[type="checkbox"]:checked').each(function () {
                roleitem = {};
                roleitem['role'] = $(this).data('set');
                roleitem['permission'] = role_id;
                rpid.push(roleitem);
            });
            console.log(rpid);
            $("#myModal").modal("hide");
        }

    }

});

Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');

new Vue({
    el: '#id_role',

    data: {
        newRole: {
            name: '',
            label: ''
        },

        submitted: false
    },

    computed: {
        errors: function () {
            for (var key in this.newRole) {
                if (! this.newRole[key]) return true;
            }
            return false;
        }
    },

    ready: function() {
        this.fetchRoles();
    },

    methods: {
        fetchRoles: function() {
            // role is array from database
            this.$http.get('api/roles', function (role) {
                this.$set('role', role);
            });
        },

        onSubmitForm: function(e) {
            // prevent the default action
            e.preventDefault();
            // variable need for method post
            var role = this.newRole;
            // add new role to role array
            this.role.push(this.newRole);
            // reset input values
            this.newRole = { name: '', label: ''};
            // show added message
            this.submitted = true;
            // send post ajax request
            this.$http.post('api/roles', role);
        }

    }

});

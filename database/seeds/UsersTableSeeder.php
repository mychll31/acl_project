<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        DB::table('users')->insert([
            [
                'empno' => '0001',
                'name' => $faker->name,
                'username' => $faker->userName,
                'email' => $faker->unique()->email,
                'PASSWORD' => bcrypt('secret')
            ],
            [
                'empno' => '0002',
                'name' => $faker->name,
                'username' => $faker->userName,
                'email' => $faker->unique()->email,
                'PASSWORD' => bcrypt('secret')
            ]
        ]);
    }
}

var elixir = require('laravel-elixir');

elixir(function(mix){
    /** Login */
    mix.styles([
        '../global/plugins/font-awesome/css/font-awesome.min.css',
        '../global/plugins/simple-line-icons/simple-line-icons.min.css',
        '../global/plugins/bootstrap/css/bootstrap.min.css',
        '../global/plugins/uniform/css/uniform.default.css',
        '../global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
        '../pages/css/login.min.css',
        '../global/plugins/select2/css/select2.min.css',
        '../global/plugins/select2/css/select2-bootstrap.min.css',
        '../global/css/components.min.css',
        '../global/css/plugins.min.css'
    ], 'public/css/login.min.css');

    /** Modal jquery */
    mix.scripts([
        '../global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js',
        '../global/plugins/bootstrap-modal/js/bootstrap-modal.js',
        '../pages/scripts/ui-extended-modals.min.js'
    ], 'public/js/modal.min.js');

    mix.styles([
        '../global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css',
        '../global/plugins/bootstrap-modal/css/bootstrap-modal.css'
    ], 'public/js/modal.min.css');

    /** datatables */
    mix.scripts([
        '../global/scripts/datatable.js',
        '../global/plugins/datatables/datatables.min.js',
        '../global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'
        ], 'public/js/datatables.min.js');

    mix.styles([
        '../global/plugins/datatables/datatables.min.css',
        '../global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
        ], 'public/css/datatables.min.css');

    /** core plugins */
    mix.scripts([
        '../global/plugins/jquery.min.js',
        '../global/plugins/bootstrap/js/bootstrap.min.js',
        '../global/plugins/js.cookie.min.js',
        '../global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
        '../global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
        '../global/plugins/jquery.blockui.min.js',
        '../global/plugins/uniform/jquery.uniform.min.js',
        '../global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
        '../global/scripts/app.min.js'
        ], 'public/js/core.min.js');

    /** layout scripts */
    mix.scripts([
        '../layouts/layout/scripts/layout.min.js',
        '../layouts/layout/scripts/demo.min.js',
        '../layouts/global/scripts/quick-sidebar.min.js'
        ], 'public/js/layouts.min.js');

    /** Vue JS */
    mix.scripts([
        'vendor/vue.min.js',
        'vendor/vue-resource.min.js',
    ], 'public/js/vendor.js');

});
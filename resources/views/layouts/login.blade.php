<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- begin head -->
<head>
    <meta charset="utf-8" />
    <title>AMS | User Login </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- begin mandatory styles -->
    <link href="{{ asset('../resources/assets/metronic/css/mandatory.min.css') }}" rel="stylesheet">
    <!-- end mandatory styles -->

    <!-- begin login styles -->
    <link href="{{ asset('css/login.min.css') }}" rel="stylesheet">
    <!-- end login styles -->

    <link rel="shortcut icon" href="favicon.ico" /> </head>
<!-- end head -->

<body class=" login">
<div class="menu-toggler sidebar-toggler"></div>
<!-- END SIDEBAR TOGGLER BUTTON -->

<!-- BEGIN LOGIN -->
<div class="content">
    @yield('content')
</div>
<!--[if lt IE 9]>
<script src="{{ asset('../resources/assets/metronic/js/ie.min.js') }}" type="text/javascript"></script>
<![endif]-->
<!-- begin core plugins -->
<script src="{{ asset('../resources/assets/metronic/js/mandatory.min.js') }}" type="text/javascript"></script>
<!-- end core plugins -->

<!-- begin login js -->
<script src="{{ asset('../resources/assets/metronic/login/js/login.min.js') }}" type="text/javascript"></script>
<!-- end login js -->
</body>

</html>
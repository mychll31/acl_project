<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <li class="sidebar-toggler-wrapper hide">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler"> </div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>
            <li class="sidebar-search-wrapper">
                <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                <form class="sidebar-search  " action="page_general_search_3.html" method="POST">
                    <a href="javascript:;" class="remove">
                        <i class="icon-close"></i>
                    </a>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search...">
                                    <span class="input-group-btn">
                                        <a href="javascript:;" class="btn submit">
                                            <i class="icon-magnifier"></i>
                                        </a>
                                    </span>
                    </div>
                </form>
                <!-- END RESPONSIVE QUICK SEARCH FORM -->
            </li>

            <li class="nav-item start">
                <a href="javascript:;" class="nav-link">
                    <i class="fa fa-upload"></i>
                    <span class="title">Upload</span>
                    <span class=""></span>
                </a>
            </li>
            <li class="nav-item">
                <a href="javascript:;" class="nav-link">
                    <i class="fa fa-calendar"></i>
                    <span class="title">Timesheet</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="javascript:;" class="nav-link">
                    <i class="fa fa-list-alt"></i>
                    <span class="title">Team</span>
                </a>
            </li>
            <li class="heading">
                <h3 class="uppercase">Roles and Permissions</h3>
            </li>
            <li class="nav-item @yield('roles_permission')">
                <a href="{{ url('permissions') }}" class="nav-link">
                    <i class="fa fa-unlock-alt"></i>
                    <span class="title">Permission - Role</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="javascript:;" class="nav-link">
                    <i class="fa fa-user"></i>
                    <span class="title">Role - User</span>
                </a>
            </li>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->
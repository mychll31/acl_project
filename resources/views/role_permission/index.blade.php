@extends('layouts.app')
@section('title', 'Permission')
@section('page_title', 'Permission')
@section('sub_title', ' Manage Permission')
@section('roles_permission', 'active')
@push('head')
<meta id="token" name="token" value="{{ csrf_token() }}">
<link href="{{ asset('css/datatables.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/modal.min.css') }}" rel="stylesheet">
@endpush
@section('content')
    <style>
        .error {
            font-weight:bold;
            color: red; }
    </style>
    <div id="id_permission">
        <!-- Begin Permissions -->
        <div class="col-xs-6">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-lock font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"> Permissions</span>
                    </div>
                </div>
                <div id='permissions' class="portlet-body form">
                    {!! Form::open(array('role'=>'form', 'class'=>'form-horizontal', 'method'=>'POST', 'v-on'=>'submit: onSubmitForm')) !!}
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">
                                Name
                                <span class="error" v-if="! newPermission.name">*</span>
                            </label>
                            <div class="col-md-9">
                                {!! Form::text('name', null, array('class'=>'form-control', 'placeholder'=>'edit_form', 'id'=>'name', 'v-model'=>'newPermission.name')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">
                                Label
                                <span class="error" v-if="! newPermission.label">*</span>
                            </label>
                            <div class="col-md-9">
                                {!! Form::text('label', null, array('class'=>'form-control', 'placeholder'=>'Edit the form', 'id'=>'label', 'v-model'=>'newPermission.label')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            {!! Form::submit('Submit', ['class'=>'btn green', 'v-attr'=>'disabled: errors']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <br>
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet box red">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>List </div>
                            <div class="actions">
                                <a href="javascript:;" class="btn btn-default btn-sm">
                                    <i class="fa fa-edit"></i> Edit </a>
                            </div>
                        </div>
                        <div class="portlet-body" id="users-table">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_3">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th> Name </th>
                                    <th> Label </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="odd gradeX" v-repeat="permission">
                                    <td>
                                        <input type="radio" id="@{{ id }}" />
                                    </td>
                                    <td>@{{ name }}</td>
                                    <td>@{{ label }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
        <!-- End Permissions -->

        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Choose Permission</h4>
              </div>
              <div class="modal-body">
                <ul class="ul_custom">
                    <li class="list-group-item"
                        v-repeat="pe: permission">
                        <button v-if="pe.id != role_id" class="li_button_check chk_red" v-on="click: addRole(pe.id)">&#10004</button>
                        <button v-if="pe.id == role_id" class="li_button_check chk_green" v-on="">&#10004</button>
                        <b>@{{ pe.name | uppercase }}</b> @{{ pe.label }}
                    </li>
                </ul>
                <input type="text" v-model='role_id'>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" v-on="click: saveRoles">Save changes</button>
              </div>
            </div>
          </div>
        </div>
        <!-- End Modal -->
    </div>

    <div id="id_role">
        <div class="col-xs-6">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-lock font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"> Roles </span>
                    </div>
                </div>
                <div id='roles' class="portlet-body form">
                    {!! Form::open(array('role'=>'form', 'class'=>'form-horizontal', 'method'=>'POST', 'v-on'=>'submit: onSubmitForm')) !!}
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">
                                Name
                                <span class="error" v-if="! newRole.name">*</span>
                            </label>
                            <div class="col-md-9">
                                {!! Form::text('name', null, array('class'=>'form-control', 'placeholder'=>'manager', 'id'=>'name', 'v-model'=>'newRole.name')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">
                                Label
                                <span class="error" v-if="! newRole.label">*</span>
                            </label>
                            <div class="col-md-9">
                                {!! Form::text('label', null, array('class'=>'form-control', 'placeholder'=>'Site Manager', 'id'=>'label', 'v-model'=>'newRole.label')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            {!! Form::submit('Submit', ['class'=>'btn green', 'v-attr'=>'disabled: errors']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <br>
                    <!-- begin table roles-->
                    <div class="portlet light portlet-fit portlet-datatable bordered" id="form_wizard_1">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-layers font-green"></i>
                                <span class="caption-subject font-green sbold uppercase">Roles</span>
                            </div>
                            <div class="actions">
                                <a class="btn btn-circle btn-icon-only btn-default" data-toggle="modal" data-target="#myModal">
                                    <i class="fa fa-user-plus"></i>
                                </a>
                                <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                    <i class="icon-trash"></i>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_2">
                                <thead>
                                    <tr>
                                        <th class="table-checkbox">
                                            <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" /> </th>
                                        <th> Name </th>
                                        <th> Label </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd gradeX" v-repeat="role">
                                        <td><input id="chkRole" type="checkbox" class="checkboxes" value="1" data-set="@{{ id }}" /></td>
                                        <td>@{{ name }}</td>
                                        <td>@{{ label }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- End table roles -->
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script src="js/vendor.js"></script>
<script src="js/permissions.js"></script>
<script src="js/roles.js"></script>
<script src="js/datatables.min.js"></script>
<!-- DataTables -->
<!-- <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script> -->
<script src="{{ asset('../resources/assets/pages/scripts/table-datatables-managed.min.js') }}" type="text/javascript"></script>
@endpush


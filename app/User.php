<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    use HasRole;

    protected $fillable = [
        'name', 'email', 'PASSWORD',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'PASSWORD', 'remember_token',
    ];

    /**
     * use 'PASSWORD' instead of 'password'
     * Auth/Authenticatable.php
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->PASSWORD;
    }

    public function roles() 
    {
        return $this->belongsToMany(Role::class);
    }

}

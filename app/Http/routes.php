<?php

use App\Permission;
use App\Role;

Route::get('/', function () {
    return view('welcome');
});

Route::auth();
Route::get('/home', 'HomeController@index');

/** Permission */
Route::get('permissions',       function()  { return view('role_permission.index'); });
Route::get('api/permissions',   function () { return Permission::all(); });
Route::post('api/permissions',  function () { return Permission::Create(Request::all()); });
/** Role */
Route::get('api/roles',   function () { return Role::all(); });
Route::post('api/roles',  function () { return Role::Create(Request::all()); });

/** permission_role */
Route::post('api/permission_role',  function () { dd(Request::all()); });

/** Datatables */
Route::controller('datatables', 'DatatablesController', [
    'anyData'  => 'datatables.data',
    'getIndex' => 'datatables',
]);
<?php

namespace App;

trait HasRole {
    /** User and Role relationship */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /** Assigning role to user */
    public function assignRole($role)
    {
        return $this->roles()->save(
            Role::whereName($role)->firstorFail()
        );
    }

    public function hasRole($role)
    {
        if(is_string($role)){
            return $this->roles->contains('name', $role);
        }

        return $role->intersect($this->roles);
    }
}